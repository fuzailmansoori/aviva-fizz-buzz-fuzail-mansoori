﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvivaFizzBuzz.Web.Repository
{
    public class FizzRepository
    {
        public bool Calculate(int number)
        {
            bool flagResult = false;
            if (number % 3 == 0)
                flagResult = true;
            else
                flagResult = false;

            return flagResult;
        }

        public string GetMessage()
        {
            return "Fizz";
        }

    }
}