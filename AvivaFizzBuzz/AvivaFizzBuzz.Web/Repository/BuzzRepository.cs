﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvivaFizzBuzz.Web.Repository
{
    public class BuzzRepository
    {
        public bool Calculate(int number)
        {
            bool flagResult = false;
            if (number % 5 == 0)
                flagResult = true;
            else
                flagResult = false;

            return flagResult;
        }

        public string GetMessage()
        {
            return "Buzz";
        }

    }
}