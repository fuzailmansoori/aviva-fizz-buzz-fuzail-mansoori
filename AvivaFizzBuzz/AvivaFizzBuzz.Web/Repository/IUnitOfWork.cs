﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AvivaFizzBuzz.Web.Models;

namespace AvivaFizzBuzz.Web.Repository
{
    public interface IUnitOfWork
    {
        List<FizzBuzzDto> GetAllFizzBuzz(int number);
    }
}
