﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using AvivaFizzBuzz.Web.Models;

namespace AvivaFizzBuzz.Web.Repository
{

    public class UnitOfWork : IUnitOfWork
    {

        private FizzRepository fizzRepo;
        private BuzzRepository buzzRepo;
        private FizzBuzzRepository fizzbuzzRepo;

        public FizzRepository FizzRepository
        {
            get
            {

                if (fizzRepo == null)
                {
                    fizzRepo = new FizzRepository();
                }
                return fizzRepo;
            }
        }

        public BuzzRepository BuzzRepository
        {
            get
            {

                if (buzzRepo == null)
                {
                    buzzRepo = new BuzzRepository();
                }
                return buzzRepo;
            }
        }

        public FizzBuzzRepository FizzBuzzRepository
        {
            get
            {

                if (fizzbuzzRepo == null)
                {
                    fizzbuzzRepo = new FizzBuzzRepository();
                }
                return fizzbuzzRepo;
            }
        }

        public List<FizzBuzzDto> GetAllFizzBuzz(int number)
        {
            List<FizzBuzzDto> listofFizzBuzz = new List<FizzBuzzDto>();
            string messageResult = string.Empty;

            for (int count = 1; count <= number; count++)
            {
                FizzBuzzDto objFizzBuzz = new FizzBuzzDto();

                if (FizzBuzzRepository.Calculate(count))
                {
                    messageResult = ReplaceFizzBuzzChar(FizzBuzzRepository.GetMessage());
                    objFizzBuzz.Output = messageResult;
                    objFizzBuzz.color = "Red";
                    messageResult = "";
                    listofFizzBuzz.Add(objFizzBuzz);
                }
                else if (BuzzRepository.Calculate(count))
                {
                    messageResult = ReplaceFizzBuzzChar(BuzzRepository.GetMessage());
                    objFizzBuzz.Output = messageResult;
                    objFizzBuzz.color = "Green";
                    messageResult = "";
                    listofFizzBuzz.Add(objFizzBuzz);

                }
                else if (FizzRepository.Calculate(count))
                {
                    messageResult = ReplaceFizzBuzzChar(FizzRepository.GetMessage());
                    objFizzBuzz.Output = messageResult;
                    objFizzBuzz.color = "Blue";
                    listofFizzBuzz.Add(objFizzBuzz);

                }
                else
                {

                    objFizzBuzz.Output = count.ToString();
                    listofFizzBuzz.Add(objFizzBuzz);
                }
            }


            return listofFizzBuzz;
        }

        public string ReplaceFizzBuzzChar(string fizzBuzz)
        {
            string charDdayOfWeek = DateTime.Now.DayOfWeek.ToString().Substring(0, 1);
            string result = string.Empty;

            if (fizzBuzz.Contains("Fizz Buzz"))
            {
                result = fizzBuzz.Replace("F", charDdayOfWeek);
                result = result.Replace("B", charDdayOfWeek);
            }
            else if (fizzBuzz.Contains("Fizz"))
            {
                result = fizzBuzz.Replace("F", charDdayOfWeek);
            }
            else if (fizzBuzz.Contains("Buzz"))
            {
                result = fizzBuzz.Replace("B", charDdayOfWeek);
            }
            else
            {
                result = fizzBuzz;
            }
            return result;

        }

        public string ValidateNumber(int number)
        {
            if (number < 1 || number > 1000)
            {
                throw new IndexOutOfRangeException("The number supplied is out of the expected range (1 - 1000).");
            }

            return string.Empty;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

    }
          
   
}
