﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvivaFizzBuzz.Web.Repository
{
    public class FizzBuzzRepository
    {
        public bool Calculate(int number)
        {
            bool flagResult = false;
            if (number % 3 == 0 && number % 5 == 0)
                flagResult = true;
            else
                flagResult = false;

            return flagResult;
        }


        public string GetMessage()
        {
            return "Fizz Buzz";
        }

    }
}