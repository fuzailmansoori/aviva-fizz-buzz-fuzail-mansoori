﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AvivaFizzBuzz.Web.Repository;
using AvivaFizzBuzz.Web.Models;
using PagedList;
using PagedList.Mvc;

namespace AvivaFizzBuzz.Web.Controllers
{
  
        /// <summary>
        /// Controller for viewing the list of Fizz Buzz Results
        /// </summary>

        public class FizzBuzzController : Controller
        {

            private IUnitOfWork UnitOfWork;

            public FizzBuzzController(IUnitOfWork repo)
            {
                UnitOfWork = repo;
            }


            /// <summary>
            /// Displays a list of Results by page Number
            /// </summary>
            /// <param name="page">The integer page Number</param>

            [HttpGet]
            public ActionResult Index(int? page)
            {
                FizzBuzzViewModel objmodel = null;
                if (TempData["FizzBuzzResult"] != null)
                {
                    int pageNumber = (page ?? 1);
                    int pageSize = 20;
                    var result = (List<FizzBuzzDto>)TempData["FizzBuzzResult"];
                    TempData.Keep("FizzBuzzResult");
                    objmodel = new FizzBuzzViewModel();
                    objmodel.PagedLists = result.ToPagedList(pageNumber, pageSize);
                    return View(objmodel);
                }


                return View(objmodel);
            }

            /// <summary>
            /// Displays a list of All Results by entering inputNumber
            /// </summary>
            /// <param name="model">model is FizzBuzzViewModel for strongly typed Views </param>
            /// <param name="submitButton"> submitButton is a string to determine httpPost submit button name</param>


            [HttpPost]
            public ActionResult Index(FizzBuzzViewModel model)
            {
                int pageNumber = 1;
                int pageSize = 20;
                FizzBuzzViewModel objmodel = null;
                if (ModelState.IsValid)
                {
                    var result = UnitOfWork.GetAllFizzBuzz(model.InputNumber);
                    objmodel = new FizzBuzzViewModel();
                    objmodel.PagedLists = result.ToPagedList(pageNumber, pageSize);
                    TempData["FizzBuzzResult"] = result;
                }
                return View(objmodel);


            }





        }

   
}
