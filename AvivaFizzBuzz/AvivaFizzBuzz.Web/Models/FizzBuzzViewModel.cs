﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using PagedList;
using PagedList.Mvc;

namespace AvivaFizzBuzz.Web.Models
{
    public class FizzBuzzViewModel
    {
        [Required(ErrorMessage = "Enter Number")]
        [Range(1, 1000, ErrorMessage = "Enter Number between 1 to 1000")]
        public int InputNumber { get; set; }
        public IPagedList<FizzBuzzDto> PagedLists { get; set; } 
    }
}