﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AvivaFizzBuzz.Web.Repository;

namespace AvivaFizzBuzz.UnitTest
{
    [TestClass]
    public class BuzzRepositoryTest
    {

        private BuzzRepository buzzRepository;

        [TestInitialize]
        public void Setup()
        {
            buzzRepository = new BuzzRepository();
        }


        [TestMethod]
        public void Passing_number_Five_Returns_Output_Boolean_True()
        {
            bool expected = true;
            var actual = buzzRepository.Calculate(5);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void Methods_Returns_Output_String_Buzz()
        {
            string expected = "Buzz";
            var actual = buzzRepository.GetMessage();
            Assert.AreEqual(expected, actual);
        }



    }
}
