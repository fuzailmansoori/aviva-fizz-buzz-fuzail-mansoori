﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AvivaFizzBuzz.Web.Repository;

namespace AvivaFizzBuzz.UnitTest
{
    [TestClass]
    public class UnitOfWorkTest
    {
        private UnitOfWork unitOfWork;

        [TestInitialize]
        public void Setup()
        {
            unitOfWork = new UnitOfWork();
        }

        [TestMethod]
        public void Passing_Number_Fifteen_Returns_Output_Items_Count_Fifteen()
        {
            int expected = 15;
            var actual = unitOfWork.GetAllFizzBuzz(15);
            Assert.AreEqual(expected, actual.Count);
        }


        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void Passing_Number_Greater_Than_OneThousand_Throws_IndexOutOfRangeException()
        {

            unitOfWork.ValidateNumber(1001);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void Passing_Number_Less_Than_One_Throws_IndexOutOfRangeException()
        {
            unitOfWork.ValidateNumber(-1);
        }


    }
}
