﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using AvivaFizzBuzz.Web.Controllers;
using AvivaFizzBuzz.Web.Models;
using Moq;
using AvivaFizzBuzz.Web.Repository;


namespace AvivaFizzBuzz.UnitTest
{
    [TestClass]
    public class FizzBuzzControllerTest
    {

        private Mock<IUnitOfWork> unitOfWorkMock;
        FizzBuzzController objController;
        List<FizzBuzzDto> listFizzBuzz;

        [TestInitialize]
        public void Initialize()
        {

            unitOfWorkMock = new Mock<IUnitOfWork>();
            objController = new FizzBuzzController(unitOfWorkMock.Object);

            listFizzBuzz = new List<FizzBuzzDto>();
            listFizzBuzz.Add(new FizzBuzzDto() { color = "", Output = "1" });
            listFizzBuzz.Add(new FizzBuzzDto() { color = "", Output = "2" });
            listFizzBuzz.Add(new FizzBuzzDto() { color = "blue", Output = "Fizz" });

        }


        [TestMethod]
        public void Index_ActionResult_Returns_ViewResult()
        {

            // Act
            var result = objController.Index(1) as ViewResult;

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }



        [TestMethod]
        public void Index_ActionResult_Returns_Index_View()
        {

            // Act
            var result = objController.Index(1) as ViewResult;

            //Assert
            Assert.AreEqual("", result.ViewName);
        }



        [TestMethod]
        public void Index_ActionResult_Returns_Lists_Of_Items()
        {

            //Arrange               
            unitOfWorkMock.Setup(x => x.GetAllFizzBuzz(It.IsAny<int>())).Returns(listFizzBuzz);

            FizzBuzzViewModel objmodel = new FizzBuzzViewModel();
            objmodel.InputNumber = 7;

            //Act
            var result = objController.Index(objmodel) as ViewResult;
            var ModelList = (FizzBuzzViewModel)result.ViewData.Model;

            //Assert
            Assert.IsNotNull(ModelList, "The list of fizzBuzz does not exist");

        }



    }
}