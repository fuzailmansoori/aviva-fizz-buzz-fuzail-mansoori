﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AvivaFizzBuzz.Web.Repository;

namespace AvivaFizzBuzz.UnitTest
{
    [TestClass]
    public class FizzBuzzRepositoryTest
    {
        private FizzBuzzRepository fizzBuzzRepository;

        [TestInitialize]
        public void Setup()
        {
            fizzBuzzRepository = new FizzBuzzRepository();
        }

        [TestMethod]
        public void Passing_Number_Fifteen_Returns_Output_Boolean_True()
        {
            bool expected = true;
            var actual = fizzBuzzRepository.Calculate(15);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Methods_Returns_Output_String_FizzBuzz()
        {
            string expected = "Fizz Buzz";
            var actual = fizzBuzzRepository.GetMessage();
            Assert.AreEqual(expected, actual);
        }

    }
}
